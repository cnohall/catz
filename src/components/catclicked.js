import React, {Component} from 'react';
import App from '../App';
import ReactDOM from 'react-dom'


export default class ViewOneCat extends Component {

    constructor (props) {
      super(props);
        this.state = {
            cat : props.cat,
        };
    }

    returnToHomepage (){       
        const view = <App/>
        ReactDOM.render(view, document.getElementById('root'));
    }
    
    render () {
        const cat = this.state.cat;
        return (
        <div>
        <div className = "onecat">
                <img className="oneCatImage"src={"/images/"+cat.image} alt="Bild" />
                    <h6>The catz name: {cat.name}</h6>
                    <h6>Wow, such cute: {cat.cutenessLevel}</h6>
                    <h6>Allergy Danger: {cat.allergyInducingFur.toString()}</h6>
                    <h6>Lives left: {cat.livesLeft}</h6>
                    <button className="button" onClick={()=>this.returnToHomepage()}>Return</button>
          </div>
          </div>
        );
    }
  }