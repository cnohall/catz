import React from 'react';
import './App.css';
import axios from 'axios'
import ViewOneCat from './components/catclicked'
import ReactDOM from 'react-dom';
export default class App extends React.Component {
  constructor () {
    super();
      this.state = {
          catz: [],
          originalOrder: [],
          isLoading: false,
        };
      this.defaultSort = this.defaultSort.bind(this);
  }
  
  componentDidMount() {
    this.setState({ isLoading: true });
    axios.get('https://cors-anywhere.herokuapp.com/https://dev-airlock-261607.appspot.com/')
    .then(res => {
      this.setState({ catz: res.data.cats, isLoading: false })
      let originalOrder = res.data.cats.map(function (cat) { return cat });
      this.setState({originalOrder})

    });
  }

  oneCatClicked (cat){       
    const view = <ViewOneCat cat={cat}/>
    ReactDOM.render(view, document.getElementById('root'));
  }


  ascending (array, key) {
    this.setState({ isLoading: true })
    let catz = array.sort(function(a, b) {
      let x = a[key]; let y = b[key];
      return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
    this.setState({ catz: catz, isLoading: false  })
  }

  descending (array, key) {
    this.setState({ isLoading: true })
    let catz = array.sort(function(a, b) {
      let x = a[key]; let y = b[key];
      return ((x > y) ? -1 : ((x < y) ? 1 : 0));
    });
    this.setState({ catz, isLoading: false  })
  }
  

  defaultSort () {
    this.setState({ isLoading: true })
    this.setState({ catz: [...this.state.originalOrder], isLoading: false  })
  }
  
  render () {
      const { catz, isLoading} = this.state;
      if (isLoading) {
        return (
        <div>
        <h1>Loading ...</h1>
        </div>
        );
      } else {
        return (
          catz.length > 0 && (
            <div>
            <div className="nav-title">
                Wow so cute
            </div>
            <div className="nav">
            <div className="nav-links">
                <button className="button" onClick={()=>this.defaultSort()}>None</button>
                <button className="button" onClick={()=>this.descending(catz, "cutenessLevel")}>Much Cute</button>
                <button className="button" onClick={()=>this.ascending(catz, "cutenessLevel")}>Not Cute</button>
                <button className="button" onClick={()=>this.descending(catz, "livesLeft")}>Much Life</button>
                <button className="button" onClick={()=>this.ascending(catz, "livesLeft")}>Little Life</button>
            </div>
            </div>
                <div className = "catList">
                    {catz.map(cat =>
                        <div key={cat.name} className = "cat" onClick={() => this.oneCatClicked(cat)}>
                            <img src={"/images/"+cat.image} alt="Bild" width="200" height="200"/>
                        <div className= "name">
                            {cat.name}
                        </div>
                    </div>
                    )}
                </div>
            </div>
          )
        );
      }
  }
}
